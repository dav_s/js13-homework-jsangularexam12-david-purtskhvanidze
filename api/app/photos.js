const path = require('path');
const fs = require("fs").promises;
const express = require('express');
const multer = require('multer');
const { nanoid } = require('nanoid');
const config = require('../config');
const Photo = require("../models/Photo");
const mongoose = require("mongoose");
const auth = require("../middleware/auth");


const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {

    const query = {};

    if (req.query.user) {
      query.user = req.query.user;
    }

    const photos = await Photo.find(query).populate('user');

    return res.send(photos);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const photos = await Photo.findById(req.params.id);

    if (!photos) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(photos);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.file) {
      return res.status(400).send({message: 'Title and Img are required'});
    }

    const photoData = {
      user: req.user.id,
      title: req.body.title,
      image: req.file.filename,
    };

    const photo = new Photo(photoData);

    await photo.save();

    return res.send({message: 'Created new photo', id: photo._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      if (req.file) {
        await fs.unlink(req.file.path);
      }

      return res.status(400).send(e);
    }

    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    const photo = await Photo.findById(req.params.id);

    if (!photo) {
      return res.status(404).send({message: 'Not found'});
    }

    const imgPath = config.uploadPath + '/' + photo.image

    await Photo.findByIdAndDelete(req.params.id);
    await fs.unlink(imgPath);
    const message = {message: `Photo with id ${req.params.id} is deleted`};

    return res.send(message);
  } catch (e) {
    next(e);
  }
});

module.exports = router;