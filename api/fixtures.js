const mongoose = require('mongoose');
const config = require("./config");
const Photo = require("./models/Photo");
const User = require("./models/User");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2] = await User.create({
    email: 'user@user.com',
    password: '123',
    token: nanoid(),
    role: 'user'
  }, {
    email: 'user1@user1.com',
    password: '123',
    token: nanoid(),
    role: 'user'
  });

  await Photo.create({
    user: user1,
    title: 'User photo 1',
    image: 'photo1.jpeg'
  }, {
    user: user1,
    title: 'User photo 2',
    image: 'photo2.jpeg'
  }, {
    user: user1,
    title: 'User photo 3',
    image: 'photo3.png'
  }, {
    user: user2,
    title: 'User photo 4',
    image: 'photo4.jpeg'
  }, {
    user: user2,
    title: 'User photo 5',
    image: 'photo5.jpeg'
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));