import { createReducer, on } from '@ngrx/store';
import {
  createPhotoFailure,
  createPhotoRequest,
  createPhotoSuccess,
  deletePhotoFailure,
  deletePhotoRequest, deletePhotoSuccess,
  fetchPhotosByUserFailure,
  fetchPhotosByUserRequest,
  fetchPhotosByUserSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess
} from './photos.actions';
import { PhotosState } from './types';

const initialState: PhotosState = {
  photos: [],
  fetchLoading: false,
  fetchError: null,
  photosByUser: [],
  fetchPhotosByUserLoading: false,
  fetchPhotosByUserError: null,
  createLoading: false,
  createError: null,
};

export const photosReducer = createReducer(
  initialState,
  on(fetchPhotosByUserRequest, state => ({...state, fetchLoading: true})),
  on(fetchPhotosByUserSuccess, (state, {photos}) => ({
    ...state,
    fetchLoading: false,
    photos
  })),
  on(fetchPhotosByUserFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(fetchPhotosRequest, state => ({...state, fetchLoading: true})),
  on(fetchPhotosSuccess, (state, {photos}) => ({
    ...state,
    fetchLoading: false,
    photos
  })),
  on(fetchPhotosFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(createPhotoRequest, state => ({...state, createLoading: true})),
  on(createPhotoSuccess, state => ({...state, createLoading: false})),
  on(createPhotoFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),

  on(deletePhotoRequest, state => ({...state, createLoading: true})),
  on(deletePhotoSuccess, state => ({...state, createLoading: false})),
  on(deletePhotoFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  }))
);
