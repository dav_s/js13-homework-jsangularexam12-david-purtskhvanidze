import { LoginError, RegisterError, User } from '../models/user.model';
import { Photo } from '../models/photo.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type PhotosState = {
  photos: Photo[],
  fetchLoading: boolean,
  fetchError: null | string,
  photosByUser: Photo[],
  fetchPhotosByUserLoading: boolean,
  fetchPhotosByUserError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type AppState = {
  users: UsersState,
  photos: PhotosState,
  photosByUser: PhotosState,
}
