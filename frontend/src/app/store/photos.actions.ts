import { createAction, props } from '@ngrx/store';
import { Photo, PhotoData } from '../models/photo.model';

export const fetchPhotosByUserRequest = createAction(
  '[Photos] Fetch Request',
  props<{id: string}>());
export const fetchPhotosByUserSuccess = createAction(
  '[Photos] Fetch Success',
  props<{photos: Photo[]}>()
);
export const fetchPhotosByUserFailure = createAction(
  '[Photos] Fetch Failure',
  props<{error: string}>()
);

export const fetchPhotosRequest = createAction('[Photos] Fetch Request');
export const fetchPhotosSuccess = createAction(
  '[Photos] Fetch Success',
  props<{photos: Photo[]}>()
);
export const fetchPhotosFailure = createAction(
  '[Photos] Fetch Failure',
  props<{error: string}>()
);

export const createPhotoRequest = createAction(
  '[Photos] Create Request',
  props<{photoData: PhotoData}>()
);
export const createPhotoSuccess = createAction(
  '[Photos] Create Success'
);
export const createPhotoFailure = createAction(
  '[Photos] Create Failure',
  props<{error: string}>()
);

export const deletePhotoRequest = createAction(
  '[Photos] Delete Request',
  props<{id: string}>()
);
export const deletePhotoSuccess = createAction(
  '[Photos] Delete Success'
);
export const deletePhotoFailure = createAction(
  '[Photos] Delete Failure',
  props<{error: string}>()
);
