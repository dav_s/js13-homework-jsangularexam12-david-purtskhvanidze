import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PhotosService } from '../services/photos.service';
import {
  createPhotoFailure,
  createPhotoRequest,
  createPhotoSuccess, deletePhotoFailure,
  deletePhotoRequest, deletePhotoSuccess,
  fetchPhotosByUserFailure,
  fetchPhotosByUserRequest,
  fetchPhotosByUserSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess
} from './photos.actions';
import { mergeMap, map, catchError, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';
import { HelpersService } from '../services/helpers.service';
import { Store } from '@ngrx/store';
import { AppState } from './types';
import { SocialAuthService } from 'angularx-social-login';

@Injectable()
export class PhotosEffects {

  constructor(
    private actions: Actions,
    private photosService: PhotosService,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  fetchPhotos = createEffect(() => this.actions.pipe(
    ofType(fetchPhotosRequest),
    mergeMap(() => this.photosService.getPhotos().pipe(
      map(photos => fetchPhotosSuccess({photos})),
      catchError(() => of(fetchPhotosFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchPhotosByUser = createEffect(() => this.actions.pipe(
    ofType(fetchPhotosByUserRequest),
    mergeMap(id => this.photosService.getPhotosByUser(id.id).pipe(
      map(photos => fetchPhotosByUserSuccess({photos})),
      catchError(() => of(fetchPhotosByUserFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  createPhoto = createEffect(() => this.actions.pipe(
    ofType(createPhotoRequest),
    mergeMap(({photoData}) => this.photosService.createPhoto(photoData).pipe(
      map(() => createPhotoSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createPhotoFailure({error: 'Wrong data'})))
    ))
  ));

  deletePhoto = createEffect(() => this.actions.pipe(
    ofType(deletePhotoRequest),
    mergeMap(id => this.photosService.deletePhoto(id.id).pipe(
      map(() => deletePhotoSuccess()),
      tap(async () => {
        await this.router.navigate(['/']);
        this.helpers.openSnackbar('Photo is deleted');
      }),
      catchError(() => of(deletePhotoFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

}
