import { NgModule } from '@angular/core';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { photosReducer } from './store/photos.reducer';
import { usersReducer } from './store/users.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PhotosEffects } from './store/photos.effects';
import { UsersEffects } from './store/users.effects';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

const reducers = {
  photos: photosReducer,
  photosByUser: photosReducer,
  users: usersReducer,
};

const effects = [PhotosEffects, UsersEffects];

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(effects),
  ],
  exports: [StoreModule, EffectsModule]
})
export class AppStoreModule {}
