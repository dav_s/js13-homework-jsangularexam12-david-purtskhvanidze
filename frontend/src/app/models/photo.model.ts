export class Photo {
  constructor(
    public id: string,
    public user: {_id: string, email: string},
    public title: string,
    public image: string,
  ) {}
}

export interface PhotoData {
  [key: string]: any,
  user: {_id: string, email: string},
  title: string,
  image: File | null,
}

export interface ApiPhotoData {
  _id: string,
  user: {_id: string, email: string},
  title: string,
  image: string
}
