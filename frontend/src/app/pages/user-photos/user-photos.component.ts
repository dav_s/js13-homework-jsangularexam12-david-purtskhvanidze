import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Photo, PhotoData } from '../../models/photo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ActivatedRoute, Params } from '@angular/router';
import { createPhotoRequest, deletePhotoRequest, fetchPhotosByUserRequest } from '../../store/photos.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-photos',
  templateUrl: './user-photos.component.html',
  styleUrls: ['./user-photos.component.sass']
})
export class UserPhotosComponent implements OnInit {
  photosByUser: Observable<Photo[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  user: Observable<null | User>;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.photosByUser = store.select(state => state.photosByUser.photos);
    this.loading = store.select(state => state.photosByUser.fetchPhotosByUserLoading);
    this.error = store.select(state => state.photosByUser.fetchPhotosByUserError);

    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.store.dispatch(fetchPhotosByUserRequest({id: params['id']}));
    });
  }

  delete(id: string) {
    this.store.dispatch(deletePhotoRequest({id: id}));
  }

}
