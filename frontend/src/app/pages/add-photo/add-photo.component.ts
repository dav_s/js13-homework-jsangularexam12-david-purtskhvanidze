import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PhotoData } from '../../models/photo.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { createPhotoRequest } from '../../store/photos.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.sass']
})
export class AddPhotoComponent {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(
    private store: Store<AppState>
  ) {
    this.loading = store.select(state => state.photos.createLoading);
    this.error = store.select(state => state.photos.createError);
  }


  onSubmit() {
    const photoData: PhotoData = this.form.value;
    console.log(photoData);
    this.store.dispatch(createPhotoRequest({photoData}));
  }

}
