import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchPhotosRequest } from '../../store/photos.actions';
import { Photo } from '../../models/photo.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  photos: Observable<Photo[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  constructor(private store: Store<AppState>) {
    this.photos = store.select(state => state.photos.photos);
    this.loading = store.select(state => state.photos.fetchLoading);
    this.error = store.select(state => state.photos.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPhotosRequest());
  }

}
