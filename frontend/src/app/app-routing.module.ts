import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { AddPhotoComponent } from './pages/add-photo/add-photo.component';
import { HomeComponent } from './pages/home/home.component';
import { UserPhotosComponent } from './pages/user-photos/user-photos.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'photos-by-user/:id', component: UserPhotosComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'add-photo', component: AddPhotoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
