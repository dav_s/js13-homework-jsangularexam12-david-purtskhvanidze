import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiPhotoData, Photo, PhotoData } from '../models/photo.model';
import { map } from 'rxjs/operators';
import { environment as env, environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http: HttpClient) { }

  getPhotos() {
    return this.http.get<ApiPhotoData[]>(environment.apiUrl + '/photos').pipe(
      map(response => {
        return response.map(photoData => {
          return new Photo(
            photoData._id,
            photoData.user,
            photoData.title,
            photoData.image,
          );
        });
      })
    );
  }

  getPhotosByUser(id: string) {
    return this.http.get<ApiPhotoData[]>(environment.apiUrl + '/photos?user=' + id).pipe(
      map(response => {
        return response.map(photoData => {
          return new Photo(
            photoData._id,
            photoData.user,
            photoData.title,
            photoData.image,
          );
        });
      })
    );
  }

  createPhoto(photoData: PhotoData) {
    const formData = new FormData();

    Object.keys(photoData).forEach(key => {
      if (photoData[key] !== null) {
        formData.append(key, photoData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/photos', formData);
  }

  deletePhoto(id: string) {
    return this.http.delete(env.apiUrl + '/photos/' + id);
  }
}
